# Respeaker Site

>This is a react application which visualizes in chat form what is been said through the ReSpeaker Core v2.0 and the baked in chatbalk.

## Installation

```bash
npm install
```

## Create .env

The site makes use of an .env file. In the file the API key and the ip address of the API are stored.
You have to make an .env file in the root directory. Then you can issue following commands to insert the variables.

```bash
chmod 711 dotenv
./dotenv
```

## Messages

To display the messages from the ReSpeaker Core v2.0 we have to access the API. For the chatbalk to work the MQTT broker has to be on.
It pushes the messages from the chatbalk on the topic 'command/chat'.

## Run

```bash
npm start
```

## Build

```bash
npm run build
```

## Meta

Arthur Verstraete - [ArthurVerstraete](https://github.com/ArthurVerstraete) - arthur.verstraete@student.vives.be

Frederik Feys – [FrederikFeys](https://github.com/FrederikFeys) – frederik.feys@student.vives.be
