import React, { useEffect, useState } from 'react';
import '../App.css';
import axios from 'axios';
import 'react-chatbox-component/dist/style.css';
import Sidebar from 'react-sidebar';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';  
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import grey from '@material-ui/core/colors/grey';
import { uuid } from 'uuidv4';
// import logo from '../logo.svg';

// const Publisher = require("./publisher");
var moment = require('moment');
const mqtt = require('mqtt');
const dotenv = require('dotenv');
dotenv.config();

const useStyles = makeStyles((theme) => ({
  palette: {
    primary: grey,
  },
  root: {
    flexGrow: 1,
    position: 'absolute',
    width: '100%',
    zIndex: 90,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const App = () => {

  const [robotAddress, setRobotAddress] = useState('');
  const [messages, setMessages] = useState([]);
  const [serverAddress, setServerAddress] = useState('ws://' + process.env.REACT_APP_IP + ':1884');
  const [open, setOpen] = useState(false);
  const [inputData, setInputData] = useState('');
  const [windowHeight, setWindowHeight] = useState(window.innerHeight);
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [language, setLanguage] = useState("en-US");
  let oldMessages = [];

  const classes = useStyles();

  const handleChange = (event) => {
    setRobotAddress(event.target.value);
  }

  const writeToFile = (event) => {
    event.preventDefault();
    console.log(robotAddress)

    axios.post('http://' + process.env.REACT_APP_IP + ':4000/api/robot_address?key=' + process.env.REACT_APP_API_KEY, {
      robot_address: robotAddress
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

    setRobotAddress('');
  }

  const addUserMessage = (message, bottomLine) => {
    let html = <div style={{
      backgroundColor: '#0078FF',
      color: 'white',
      width: ((windowWidth > 760) ? '38%' : '60%'),
      position: 'relative',
      left: (windowWidth > 760) ? '60%' : '38%',
      borderRadius: '15px',
      paddingTop: '3px',
      padding: '2px',
      marginRight:'20px',
      marginTop: '20px',
      marginBottom: '20px',
      textAlign: 'end',
      paddingRight: '15px',
    }}
      key={uuid().replace(/-/gi, '')}
    >
      <p style={{ fontSize: 'large' }}>{message}</p>
      <p style={{ fontStyle: 'oblique', marginBottom: '0px', textAlign: 'end', paddingBottom: '2px' }}>{bottomLine}</p>
    </div>;
    setMessages(messages => [...messages, html]);
  }

  const addResponseMessage = (message, bottomLine) => {
    let html = <div style={{
      backgroundColor: '#F1F0F0',
      color: 'black',
      width: ((windowWidth > 760) ? '40%' : '60%'),
      borderRadius: '15px',
      paddingTop: '3px',
      padding: '2px',
      marginTop: '20px',
      marginBottom: '20px',
      marginLeft: '15px',
      textAlign: 'start',
      paddingLeft: '15px',
    }}
      key={uuid().replace(/-/gi, '')}
    >
      <p style={{ fontSize: 'large' }}>{message}</p>
      <p style={{ fontStyle: 'oblique', marginBottom: '0px', textAlign: 'start', paddingBottom: '2px' }}>{bottomLine}</p>
    </div>;
    setMessages(messages => [...messages, html]);
  }

  const getMessages = () => {
      axios.get('http://' + process.env.REACT_APP_IP + ':4000/api/messages?key=' + process.env.REACT_APP_API_KEY)
        .then(function (response) {
          let data = response.data;
          for (var i = oldMessages.length; i < data.length; i++) {
            if (data[i].sender === "respeaker") {
              addUserMessage(data[i].message, data[i].confidence + " - " + moment(new Date(data[i].createdAt)).format('LLL'));
            } else {
              addResponseMessage(data[i].message, moment(new Date(data[i].createdAt)).format('LLL'));
            }
            scrollToEnd('messages');
          }
          oldMessages = data;
        })
        .catch(function (error) {
          console.log(error);
        });
  }

  const deleteAllMessages = (event) => {
    event.preventDefault();
    setMessages([]);
    axios.delete('http://' + process.env.REACT_APP_IP + ':4000/api/messages/all?key=' + process.env.REACT_APP_API_KEY)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  
  const sendMessage = () => {
    let client = mqtt.connect(serverAddress);
    client.on('connect', ()=>{
      client.publish("status", "connected");
      client.publish("command/chat", "1.0 " + inputData)
    });
    setInputData('');
  }

  const changeLanguage = (event) => {
    event.preventDefault();
    let client = mqtt.connect(serverAddress);
    client.on('connect', ()=>{
      client.publish("respeaker/config", "language:" + language)
    });
  }

  const scrollToEnd = (elementId) => {
    var elem = document.getElementById(elementId);
    elem.scrollTop = elem.scrollHeight;
  }

  useEffect(() => {
    getMessages();

    setInterval(() => {
      setWindowHeight(window.innerHeight);
      setWindowWidth(window.innerWidth);
      setServerAddress('ws://' + process.env.REACT_APP_IP + ':1884');
      // if (window.innerHeight !== windowHeight && window.innerWidth !== windowWidth) {
      //   scrollToEnd('messages');
      // }
    }, 10);

    setInterval(() => {
      getMessages();
    }, 1250)
  }, [])

  return (
    <div className="App">
      <div className={classes.root}>
      <AppBar position="static" style={{backgroundColor: '#506883'}}>
        <Toolbar>
          <Typography variant="h4" className={classes.title}>
            Message history
          </Typography>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => setOpen(!open)}>
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      </div>
      <Sidebar
        sidebar={
          <div>
            <form onSubmit={writeToFile}>
              <h3 style={{margin: '5px'}}>Fill in the IP-address of the robot</h3>
              <TextField id="ip" type="text" name="ip-address" value={robotAddress} onChange={handleChange} />
              <Button variant="contained" style={{backgroundColor: '#0078FF', color: 'white', marginLeft: '10px', margin: '5px'}} type="submit" value="Submit">
                submit
              </Button>
            </form>
            <br />
            <form onSubmit={changeLanguage}>
              <h3 style={{margin: '5px'}}>Language</h3>
              <Select
                value={language}
                onChange={(event) => setLanguage(event.target.value)}
              >
                <MenuItem value={"en-US"}>English (US)</MenuItem>
                <MenuItem value={"en-GB"}>English (UK)</MenuItem>
                <MenuItem value={"nl-BE"}>Nederlands (BE)</MenuItem>
                <MenuItem value={"fr-FR"}>Français (FR)</MenuItem>
                <MenuItem value={"de-DE"}>Deutsch (DE)</MenuItem>
              </Select>
              <Button variant="contained" style={{backgroundColor: '#0078FF', color: 'white', margin: '5px'}} type="submit">
                Change
              </Button>
            </form>
            <br />
            <form onSubmit={deleteAllMessages}>
              <br />
              <Button variant="contained" style={{backgroundColor: '#0078FF', color: 'white'}} type="submit">
                Delete all messages
              </Button>
            </form>
            <p id="api">{process.env.REACT_APP_API_KEY}</p>

          </div>
        }
        open={open}
        onSetOpen={() => setOpen(!open)}
        styles={{ sidebar: { background: '#dcdcdc', padding: 5, zIndex: 99 } }}
        pullRight={true}>
          <div id="messages" style={{ 
            backgroundColor: '#dcdcdc',
            zIndex: 1, 
            height: windowHeight - 65, 
            overflowY: 'scroll', 
            paddingTop: '60px' }}>
            {messages}
          </div>
          <div style={{
            backgroundColor: '#dcdcdc',
            height: '60px'
          }}>
            <input id="inputData" type="text" name="inputData" value={inputData} 
            onKeyDown={(event) => { if (event.keyCode === 13) { sendMessage(); return false; } }} 
            onChange={(event) => setInputData(event.target.value)} 
            style={{ 
            position: 'absolute',
            backgroundColor: '#dcdcdc',
            zIndex: 90, 
            height: 55, 
            width: '78%', 
            display: "flex", 
            justifyContent: "flex-end",
            paddingLeft: '20px',
            marginLeft: '2%',
            marginBottom: '10px',
            marginTop: 0, 
            borderWidth: '1,5px', 
            borderColor: 'gray', 
            borderStyle: 'solid', 
            bottom: 0,
            borderRadius: '15px'
               }} />
            <Button onClick={sendMessage} variant="contained" color="primary" type="submit" value="Send" 
            style={{ 
              position: 'absolute',
              bottom: '11px',
              backgroundColor: '#0078FF',
              right: '3%',
              width: '12%',
              height: 55,
              borderRadius: '15px'
              }}>
              send
              </Button>
          </div>
      </Sidebar>
    </div>

  );
}

export default App;
